var idToken = null;

function checkLogin() {
    var url_string = window.location.href;
    var url = new URL(url_string);
    idToken = url.searchParams.get("id_token");
    if (idToken != null) {
        // document.getElementById("welcomeMsg").innerHTML = "signed in";
        alert("You have successfully logged in!")
        auth();
    }
}

function auth() {
    AWS.config.update({
      region: "us-east-1",
    });

    AWS.config.credentials = new AWS.CognitoIdentityCredentials({
            IdentityPoolId : 'us-east-1:975b3797-3fce-490f-9560-4ede0e3e1372',
            Logins : {
              "cognito-idp.us-east-1.amazonaws.com/us-east-1_QxDczCDDq": idToken
            }
          });
}
