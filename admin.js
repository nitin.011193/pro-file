 window.onload=function(){
    const viewJob = document.querySelector("#view-job");
    const updateJobNumber = document.querySelector("#update-job-number");
    const updateJobStatus = document.querySelector("#update-job-status");
    const updateJobButton = document.querySelector("#update-job-button"); 
    const updateResult = document.querySelector(".update-item");
    updateJobButton.addEventListener("click",updateJob)
    viewJob.addEventListener("click",function view(){
        updateResult.innerText = "";
        fetchJob();
    });
    

    function fetchJob(){
        document.getElementById('app').innerHTML = "";
        
        fetch('https://l6r51nnz8i.execute-api.us-east-1.amazonaws.com/prod/jobs')
            .then(response => {
                if(!response.ok){
                    throw Error('Error');
                }
                return response.json();
            })
            .then(data => {
                console.log(data);
                const html = data
                .map(job =>{
                    return `
                    <div class='user'>
                    <p>ID : ${job.id}</p> 
                    <p>Name : ${job.name}</p>
                    <p>Company Name : ${job.companyname}</p> 
                    <p>Location : ${job.location}</p>
                    <p>Job Position : ${job.jobposition}</p>
                    <p>Status : ${job.jobstatus}</p>
                    </div>
                    `;
                })
                .join('')
                console.log(html);
                document
                    .querySelector('#app')
                    .insertAdjacentHTML('afterbegin',html);
            })
            .catch(error => {
                console.log(error);
            })
    }
    function updateJob(){
        fetch('https://l6r51nnz8i.execute-api.us-east-1.amazonaws.com/prod/jobs/{id}', {
        method: "PATCH",
        headers:{
            "Content-Type":"application/json"
        },
        body: JSON.stringify({
            id: updateJobNumber.value,
            jobstatus: updateJobStatus.value
        })
        })
        .then(response => {
            if(!response.ok){
                throw Error('Error');
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.log(error);
        })
        updateJobNumber.value = "";
        updateJobStatus.value= "";
        updateResult.innerHTML = "Status updated! Refresh by clicking on 'View Report'"
        fetchJob();
    }
    
    
}



