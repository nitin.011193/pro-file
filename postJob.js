
//Object-oriented programming techniques
window.onload=function(){
    
    const idNumber = document.querySelector("#id-number");
    const name = document.querySelector("#name");
    const companyName = document.querySelector("#company-name");
    const location = document.querySelector("#location");
    const jobPosition = document.querySelector("#job-position");   
    const submitButton = document.querySelector("#submit-button");
    const profileResult = document.querySelector(".result-item");
    submitButton.addEventListener("click",postJob);


    function postJob(){
        fetch('https://l6r51nnz8i.execute-api.us-east-1.amazonaws.com/prod/jobs/{id}', {
            method: "POST",
            headers:{
                "Content-Type":"application/json"
            },
            body: JSON.stringify({
                id: idNumber.value,
                name: name.value,
                companyname: companyName.value,
                location: location.value,
                jobstatus: "Pending",
                jobposition: jobPosition.value
            })
        })
        .then(response => {
            if(!response.ok){
                throw Error('Error');
            }
            return response.json();
        })
        .then(data => {
            console.log(data);
        })
        .catch(error => {
            console.log(error);
        })

        idNumber.value = "";
        companyName.value ="";
        name.value ="";
        location.value ="";
        jobPosition.value= "";
        profileResult.innerHTML = "Job submitted!"
    }
}
